FROM openjdk:16-jdk-alpine3.13

EXPOSE 25565
COPY content/startup.sh /startup.sh

CMD chmod +x /startup.sh &&\
    /startup.sh
