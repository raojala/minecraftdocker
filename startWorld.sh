#!/bin/bash

if test -z "$1" 
then
      echo "usage: startWorld.sh <worldname>"
      exit $exit_user_abort
fi

docker start $1