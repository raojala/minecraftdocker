#!/bin/bash

if test -z "$2" 
then
      echo "usage: createWorld.sh <worldname> <port>"
      exit $exit_user_abort
fi

docker run --restart always \
    --name $1 \
    -p $2:25565 \
    -v ~/$1Data/:/minecraft/ \
    -itd minecraft
