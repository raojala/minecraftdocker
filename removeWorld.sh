#!/bin/bash

if test -z "$1" 
then
      echo "usage: removeWorld.sh <worldname>"
      exit $exit_user_abort
fi

PID=$(docker exec $1 ps -ef | grep 'minecraft')
docker exec $1 kill -SIGINT $PID
docker stop $1
docker rm $1