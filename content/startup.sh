#!bin/sh
cd /
cd minecraft

SERVER=paper-1.16.5-564.jar
FILE=/minecraft/$SERVER
JAVAPATH=/opt/openjdk-16/bin/java

if test -f "$FILE"
then
    echo "$FILE exists."
else
    wget https://papermc.io/api/v2/projects/paper/versions/1.16.5/builds/564/downloads/$SERVER
fi

EULA=/minecraft/eula.txt
if test -f "$EULA"
then
    echo "$FILE exists."
else
    $JAVAPATH -Xmx1024M -Xms1024M -jar $FILE
    rm /minecraft/eula.txt
    echo eula=true >> /minecraft/eula.txt
fi

$JAVAPATH -Xmx6G -Xms6G -jar $FILE
